# Papers details

The full paper can be found on the INRIA hal science web portal.

[Manuel Poisson, Valérie Viet Triem Tong, Gilles Guette, Erwan Abgrall, Frédéric Guihéry, et al.. Unveiling stealth attack paths in Windows Environments using AWARE. CSNet 2023 - 7th Cyber Security in Networking Conference, IEEE ComSoc, Oct 2023, Montreal, Canada. pp.1-7. ⟨hal-04163780⟩](https://inria.hal.science/hal-04163780)

### abstract

When an attacker targets a system, he aims to remain undetected as long as possible. He must therefore avoid performing actions that are characteristic of an identified malicious behavior. One way to avoid detection is to only perform actions on the system that appear legitimate. That is, actions that are allowed because of the system configuration or actions that are possible by diverting the use of legitimate services. This article presents and experiments AWARE (Attacks in Windows Architectures REvealed), a defensive tool able to query a Windows system and build a directed graph highlighting possible stealthily attack paths that an attacker could use during the propagation phase of an attack campaign. These attack paths only rely on legitimate system actions and the use of Living-Off-The-Land binaries. AWARE also proposes a range of corrective measures to prevent these attack paths.

# Attack paths graph from LAB

Open [graph_LAB.html](./graph_LAB.html) in a web browser.


# Re-build LAB environment

Get virtual machine files and import them in VirtualBox.

Total size of all files: 42.6Go

## Download virtual machine files

They are splitted in 2Go files that can be re-combined.
They can be downloaded at [https://files.inria.fr/aware/](https://files.inria.fr/aware/)

## check integrity of 'split' files

#### testEnterprise

- e9b67abf2d82d068d8cb8c8e7bae60e1  testEnterpriseSplitaa
- c656af4d1c775633cb291d065de53b91  testEnterpriseSplitab
- 959279169eface9baad0a8f3f9d23aac  testEnterpriseSplitac
- 428898a9146edf959293206e0a7fc8ec  testEnterpriseSplitad
- affc372029702f5999d4b0cf6bb08c01  testEnterpriseSplitae

#### UniPalais

- 15538c5b8154912de6162c5d50392b3f  UniPalaisSplitaa
- 8bf7f9e510a68eec2709bcc36c8d754f  UniPalaisSplitab
- 1e019edb7d9cf636c6b3544d5604bf03  UniPalaisSplitac
- 878ba3b6771040f0825ca6403e15b767  UniPalaisSplitad
- bc934328e870af0f673644d6afd5a84b  UniPalaisSplitae
- 01efc349e570068952357d8404808640  UniPalaisSplitaf
- ffa16508d91226c5f49614825472ee4e  UniPalaisSplitag

#### windowsEntrepriseVm2

- f92987f70ad8addc000b7a2f3204e43a  windowsEntrepriseVm2Splitaa
- 7cfab5e4f8dedaf73b91d68438a7309a  windowsEntrepriseVm2Splitab
- 1c84a3216b28635f62a068d7f34cce21  windowsEntrepriseVm2Splitac
- 4ae94ed12a6a12681d517928dc3d3bc4  windowsEntrepriseVm2Splitad

#### windowsServer

- a1bdfbfb2d9e089916adea99dc78516a  windowsServerSplitaa
- 5308139ee9c8c1251d7a0afa22189937  windowsServerSplitab
- 8e36066e0b89e448b36847959a1417ee  windowsServerSplitac
- 65e7232c302fc10a737a656ae845dbe9  windowsServerSplitad
- 805037d9ecccf9d8accc7d29e62308ca  windowsServerSplitae

#### opForet

- 80c69e21452cb8c908aa10ef3bf04bcd  opForetSplitaa
- 0296b793472b89ce479ce63a4bec95c1  opForetSplitab
- cb34e704ed036a161d6e4dab5ee5851a  opForetSplitac


## build ova files from split:

```bash
cat testEnterpriseSplita* > testEnterprise.ova
## repeat with each 'XXSplitxx' file
```


## check integrity of .ova files

- 2ba5c5d877db706940ebdb7984b8312c  testEnterprise.ova
- 8e501cfcb0ce99b86d49091cc0c2f86b  UniPalais.ova
- c6bb9c61b125044653e5f5fe7e8f68f4  windowsEntrepriseVm2.ova
- e93056de820838340f48ae61d5ac8032  windowsServer.ova
- 344e8e62da84e75b626a7dd0dd1595e2  Op_foret.ova

## import virtual machines in virtualbox

### Network management

Before turning on any virtual machine, make sure they all have at least 1 Network adapter attached th `Host-only Adapter`

This network adapter must have 
- [x] configure adaper Manually
  - IPv4 Address: `192.168.58.1`
  - IPv4 Network Mask: `255.255.255.0`
  - IPv6 Address: `fe80::800:27ff:fe00:2`
  - IPv6 Prefix Length: `64`
- [ ] DHCP Server (**disabled**)

## Check connection workstation to/from domain controller

- On each workstation (all VM except `windows server 2019`), run the command `gpresult /r`.
- In section `PARAMÈTRES UTILISATEURS`, make sure the following lines appears:

```text
PARAMÈTRES UTILISATEURS
------------------------
    CN=Aoro Aubergiste,OU=OUglob,DC=lab,DC=local
    Heure de la dernière application de la stratégie de groupe : 17/07/2023 à 14:21:25
    Stratégie de groupe appliquée depuis :      DC01.lab.local
    Seuil de liaison lente dans la stratégie de groupe :   500 kbps
    Nom du domaine :                        LAB
    Type de domaine :                        Windows 2008 ou supérieur
```

In particular check `Stratégie de groupe appliquée depuis :      DC01.lab.local`


# Use LAB environment

You can login on machines using the following credentials

| username          | password  | member of Domain Admin |
|-------------------|-----------|-----------------------|
| aoro.aubergiste   | 234P@ss   | yes                   |
| Administrateur    | 1234P@ss  | yes                   |
| ankil.thurn       | 123P@ss   | no                    |
| essindra.essindra | 2345P@ss  | no                    |
| nillem.nillem     | 234P@ss   | no                    |
| ellana.caldin     | 2345P@ss  | no                    |
| ewilan.gilsayan   | 2345P@ss  | no                    |
| pilipip.petit     | 234P@ss   | no                    |
| sayanel.lyvant    | 234P@ss   | no                    |


# Contact

If needed or if you are interested contact manuel[dot]poisson[at]irisa[dot]fr
